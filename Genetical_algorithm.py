import numpy as np
from geneticalgorithm import geneticalgorithm as ga
from scipy.interpolate import griddata
L = 1
VOLUME = 2
W_IN = 0.12041
#w_out=0.0245
x, y, w_out= np.loadtxt("optimization_data.csv", usecols = (0, 1, 2), unpack=True, skiprows=1, delimiter=',')
x_ = np.array(x)
y_ = np.array(y)
w_ = np.array(w_out)
points = []
values = []
for i in range(len(x_)):
    xx = x_[i]
    yy = y_[i]
    zz = w_[i]
    temp_point = [xx, yy]
    points.append(temp_point)
    values.append(zz)
def interpolate_w(point):
    result = griddata(points, values, point, method='linear')
    return result
def f(x):
    """
    OF= -(w_in-w_out)*X*L
    In my future code X will change.based on w_in and w_out will change
    so every iteration will get modified w_in and w_out . and optimized X
    in that way we will get optimized Lenght of the system
    We are going to use the genetic algorithm for the best possible values after a number of generations. Crossover function in ga_supportfile means Two new (child) solutions are formed via crossover from two parent solutions by swapping one part or multiple parts of their chromosomes. Mutation in ga_supportfile means. Mutation in generates a new solution by mutating one bit or multiple bits of an existing  solution or binary string. fitness function is the function we want to maximize. in each generation it will give me solutions until it        finds best fitness function for getting maximum value
    """
    length = x[0]
    height = VOLUME/(length*L)
    point = [length, height]
    print("point val", point)
    w_out = interpolate_w(point)
    print("wout=", w_out)
    O_F = -(W_IN-w_out)*height*L
    print("of value:", O_F)
    return O_F
varbound = np.array([[0.2, 5]])
print(varbound)
vartype = np.array(['real'])
model = ga(function=f, dimension=1, variable_type_mixed=vartype, variable_boundaries=varbound)
model.run()